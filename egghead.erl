%
%	Gomoku 
%	Started: October 2014
%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-module(egghead).
-export([egghead/1,
	monte_carlo/3
		]).

-define(NBR_EPISODES, 500). % a number of episodes egghead consider before selecting a move
-define(DEPTH, 11).
-define(DEFAUL_Q_VALUE, 0).
-define(DISCOUNT, 1.0).
-define(ALPHA, 1).
-define(EPS, 0.15).



egghead(State) ->
	case moves:get_selected_moves(State) of
		[Move] ->
			%io:format("One move selected~n"), 
			Move;
		[_|_]=Moves ->
			Scores = monte_carlo(State,Moves,?NBR_EPISODES div length(Moves)),	
			% Scores = eflame:apply(?MODULE,monte_carlo,[State,Moves,?NBR_EPISODES div length(Moves)]),	
			io:format("~nScores: ~p~n",[Scores]),

			Best_moves = lists:sublist([XY || {_,XY}<-Scores],3),
			Refined = monte_carlo(State,Best_moves,?NBR_EPISODES div length(Best_moves)),	
			io:format("~nRefined: ~p~n",[Refined]),
			[{_,Move}|_] = Refined, Move
			%get_softmax_policy(State,[ XY || {_,XY}<-Refined],true)
	end.


monte_carlo({Turn,_}=State,Moves,Simulation_Nbr) ->
	lists:sort( fun({A,_},{B,_})-> A>B end,lists:foldl(
		fun(Move,Acc) ->
			CumScore = 
			lists:foldl(fun(_,ScoreAcc)-> run_episode(State,?DEPTH,Move)+ScoreAcc
						end,0,lists:seq(1,Simulation_Nbr)),
			case Turn of
				blacks -> [{-CumScore,Move}|Acc];
				whites -> [{CumScore,Move}|Acc]
			end
			
		end, [], Moves)).



run_episode(_State,0,_Move) -> 0;
run_episode(State,Depth,Move) ->
	case get_next_state_reward(State,Move) of
		{blacks_won,Reward} -> learn(State,Move,blacks_won,Reward), -1;
		{whites_won,Reward} -> learn(State,Move,whites_won,Reward), 1;
		{draw,Reward} -> learn(State,Move,draw,Reward), 0;
		{Next_state,Reward} -> 
			learn(State,Move,Next_state,Reward),
			run_episode(Next_state,Depth-1,get_policy(Next_state,false))
	end.



learn(State,Move,Next_state,Reward) ->
	case ets:lookup(egghead,{State,Move}) of
		[{_,V}] -> V1=V;
		[] -> V1=?DEFAUL_Q_VALUE
	end,
	{V2,_} = get_state_value_action(Next_state,false),
	New_value = V1+?ALPHA*(Reward + ?DISCOUNT * V2 - V1),
	case abs(New_value) < 0.1 of
		true -> ok;
		_ -> true = ets:insert(egghead,{ {State,Move}, New_value }) 
	end.



get_state_value_action(blacks_won,_) -> {0, no_move};
get_state_value_action(whites_won,_) -> {0, no_move};	
get_state_value_action(draw,_) -> {0, no_move};
get_state_value_action(State,Print) ->
	get_state_value_action(State,moves:get_selected_moves(State),Print).

get_state_value_action({Turn,_}=State,Selected_moves,Print) ->
	Rated_moves = [ case ets:lookup(egghead, {State,Move} ) of
						[] -> {?DEFAUL_Q_VALUE,Move};
						[{{State,Move},Qa}] -> {Qa,Move}
					end || Move <- Selected_moves],
	case Turn of
		whites -> {Value,_} = lists:max(Rated_moves);
		blacks -> {Value,_} = lists:min(Rated_moves)
	end,
	case Print of
		true ->	io:format("\t * * * Move values for ~p: ~p~n",[Turn,[ {V,M} || {V,M} <- Rated_moves] ]);
		_ -> ok
	end,
	Move = rand:pick_randomly([ Move || {V,Move} <- Rated_moves, V=:=Value]),
	{Value,Move}.

						


get_policy(State,Print) ->
	Moves = moves:get_selected_moves(State),
	case get_state_value_action(State,Moves,Print) of
		{0,_} -> rand:pick_randomly(Moves);
		{_,Move} -> Move
	end.

%get_policy(State,Moves,Print) ->
%	case rand:flip_coin(?EPS) of
%		false -> rand:pick_randomly(Moves);
%		_ -> 
%			{_,Move} = get_state_value_action(State,Moves,Print), 
%			Move
%	end.



get_softmax_policy(State,[M|Moves],Print) -> get_softmax_policy(State,[M|Moves],Print,M).
get_softmax_policy(State,[M|Moves],Print,FMove) ->
	case rand:flip_coin(0.8) of
		false -> M;
		_ -> get_softmax_policy(State,Moves,Print,FMove)
	end;
get_softmax_policy(_,[],_,FMove) -> FMove.




get_next_state_reward({Turn,Board},{I,J}) ->
	case element(I,element(J,Board)) of
		e -> 
			%io:format("~nNew ~p move:(~p,~p)~n",[Turn,I,J]),
			Row1 = erlang:delete_element(I,element(J,Board)),
			Board1 = erlang:delete_element(J,Board),
			case Turn of
				whites -> 
					Row2 = erlang:insert_element(I,Row1,w),
					Next_state = {blacks, erlang:insert_element(J,Board1,Row2)};

				blacks -> 
					Row2 = erlang:insert_element(I,Row1,b),
					Next_state = {whites, erlang:insert_element(J,Board1,Row2)}
			end;
			
		_ ->
			io:format("Illegal move: ~p~n",[{I,J}]),
			Next_state = illegal_move
	end,

	% set rewards
	case lines:check_five(Next_state) of
		true -> 
			{Tn,_}=Next_state,
			case Tn of
				whites -> {blacks_won,-1};
				blacks -> {whites_won,1}
			end;
		_ -> {Next_state,0}
	end.


