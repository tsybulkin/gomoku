%
%	Gomoku 
%	Started: October 2014
%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-module(gomoku).
-export([start_game/2,
		start_game/3,
		make_ply/3]).

start_game(Agent1, Agent2) ->
	start_game(Agent1,Agent2,[]).
	%eflame:apply(?MODULE,start_game,[Agent1,Agent2,[]]).

%start_game(Agent, Agent,_) -> 
%	io:format("Agents should be different~n");
start_game(Agent1, Agent2, First_moves) ->
	random:seed(now()),

	case Agent1 of
		human -> Get_whites_action = fun human/1;
		egghead -> 
			case ets:file2tab("egghead_brain.dat") of
				{ok,_} -> ok;
				{error,Reason1} ->
					io:format("egghead brain file not exists. Reason: ~p~nCreating new~n",[Reason1]),
					ets:new(egghead,[named_table])
			end,
			Get_whites_action = fun egghead:egghead/1;
		lambda ->
			case ets:file2tab("lambda_brain.dat") of
				{ok,_} -> ok;
				{error,Reason2} ->
					io:format("lambda brain file not exists. Reason: ~p~nCreating new~n",[Reason2]),
					ets:new(lambda,[named_table])
			end,
			Get_whites_action = fun lambda:lambda/1;
		random -> Get_whites_action = fun rand:rand/1
	end,

	case Agent2 of
		human -> Get_blacks_action = fun human/1;
		egghead -> 
			case ets:file2tab("egghead_brain.dat") of
				{ok,_} -> ok;
				{error,Reason3} ->
					io:format("egghead brain file not exists. Reason: ~p~nCreating new~n",[Reason3])
					%ets:new(egghead,[named_table])
			end,
			Get_blacks_action = fun egghead:egghead/1;
		lambda -> 
			case ets:file2tab("lambda_brain.dat") of
				{ok,_} -> ok;
				{error,Reason4} ->
					io:format("lambda brain file not exists. Reason: ~p~nCreating new~n",[Reason4]),
					ets:new(lambda,[named_table])
			end,
			Get_blacks_action = fun lambda:lambda/1;
		random -> Get_blacks_action = fun rand:rand/1
	end,

	Init_state = set_initial_board(First_moves),
	run_game(Init_state,Get_whites_action,Get_blacks_action),

	% clean tabs
	if
	 	Agent1==egghead orelse Agent2==egghead ->
	 		io:format("There are ~p learnt state-actions in egghead.~n",[ets:info(egghead,size)]),
			ok = ets:tab2file(egghead,"egghead_brain.dat"),
			true=ets:delete(egghead);
		true -> ok
	end,

	if
	 	Agent1==lambda orelse Agent2==lambda ->
	 		io:format("There are ~p learnt state-actions in lambda.~n",[ets:info(lambda,size)]),
			ok = ets:tab2file(lambda,"lambda_brain.dat"),
			true=ets:delete(lambda);
		true -> ok
	end.



run_game({_,Board}=State,Get_whites_action,Get_blacks_action) ->
	print_board(Board),
	case make_ply(State,Get_whites_action,Get_blacks_action) of
		blacks_won -> 
			io:format("Blacks won~n"),
			-1;
		whites_won ->
			io:format("Whites won~n"),
			1;
		draw ->
			io:format("Draw~n"),
			0;
		illegal_move ->
			io:format("Illegal move~n"),
			0;
		Next_state ->
			run_game(Next_state,Get_whites_action,Get_blacks_action)
	end.


set_initial_board(First_moves) ->
	%% set initial position
	Init_state = init_state(),
	lists:foldl(fun(Move,State)->
					change_state(State,Move)
				end, Init_state, First_moves).




make_ply({Turn,_}=State,Get_whites_action,Get_blacks_action) ->
	case Turn of
		whites -> Action = Get_whites_action(State);
		blacks -> Action = Get_blacks_action(State)
	end,

	case Action of
		{I,J} -> 
			change_state(State,{I,J});

		no_action ->
			draw;

		illegal_move ->
			case Turn of 
				whites -> blacks; 
				blacks -> whites 
			end
	end.


change_state({Turn,Board},{I,J}) ->
	case element(I,element(J,Board)) of
		e -> 
			io:format("~nNew ~p move:(~p,~p)~n",[Turn,I,J]),
			Row1 = erlang:delete_element(I,element(J,Board)),
			Board1 = erlang:delete_element(J,Board),
			case Turn of
				whites -> 
					Row2 = erlang:insert_element(I,Row1,w),
					Next_state = {blacks, erlang:insert_element(J,Board1,Row2)};

				blacks -> 
					Row2 = erlang:insert_element(I,Row1,b),
					Next_state = {whites, erlang:insert_element(J,Board1,Row2)}
			end;

		_ ->
			io:format("Illegal move: ~p~n",[{I,J}]),
			Next_state = illegal_move
	end,

	case lines:check_five(Next_state) of
		true -> 
			{Tn,_}=Next_state,
			case Tn of
				whites -> blacks_won;
				blacks -> whites_won
			end;
		_ -> Next_state
	end.



init_state() ->
	{blacks,
	{{e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e}, 
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e},
	 {e,e,e,e,e,e,e,e,e,e,e,e,e,e,e}}
	 }.





enter_move(0) -> illegal_move;
enter_move(N) ->
	case io:fread("Your move. Enter X,Y : ","~d,~d") of
		{error,_} ->
			io:format("You entered wrong numbers.~nPlease try again.~n"),
			enter_move(N-1);
		{ok,[X,Y]} when X>0,X<16,Y>0,Y<16 -> list_to_tuple([X,Y]);
		{ok,_} ->
			io:format("Numbers should be from interval [1,15].~nPlease try again.~n"),
			enter_move(N-1)
	end.


human({_,Board}) -> 
	{X,Y} = enter_move(2),
	case element(X,element(Y,Board)) of
		e -> {X,Y};
		_ -> io:format("Entered position is already occupied. Choose another one~n"),
			enter_move(1)
	end.
	

print_board(Board) ->
	Rows = tuple_to_list(Board),
	print_rows(1,Rows),
	io:format("  1 2 3 4 5 6 7 8 9 0 1 2 3 4 5~n~n").

print_rows(N,[Row|Rows]) ->
	print_stones(" " ++ integer_to_list(N rem 10), tuple_to_list(Row)),
	print_rows(N+1,Rows);
print_rows(_,[]) -> ok.
	%io:format("~n").


print_stones(Acc,[Stone|Stones]) ->
	case Stone of
		e -> print_stones("|-"++Acc,Stones);
		b -> print_stones("X-"++Acc,Stones);
		w -> print_stones("O-"++Acc,Stones)
	end;
print_stones(Acc,[]) ->
	[E1,E2,_|Tile] = lists:reverse(Acc),
	io:format("~s~n",[[E1,E2|Tile]]).


clean_tab(Tab) ->
	ets:foldl(fun({K,V},_) ->
				case abs(V) < 0.1 of
					true -> ets:delete(Tab,K);
					_ -> ok
				end 
			end, nothing, Tab).
	

