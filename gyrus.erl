%
%	Gomoku 
%	Continued: September 2015
%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-module(gyrus).
-export([start/0]).

-define(PING_FREQUENCY,1000).


start() ->
	Node = 'main@Ians-MacBook-Air.local',
	start(Node).

start(Node) ->
	case net_adm:ping(Node) of
		pang -> timer:sleep(?PING_FREQUENCY), start(Node);
		pong -> connect_main(Node)
	end.


connect_main(Node) ->
	{main,Node} ! {ready, self()},
	receive
		connected -> io:format("Connected~n"), get_workload(Node)
	after ?PING_FREQUENCY ->
		%io:format("trying to connect ...~n"),
		connect_main(Node)
	end.



get_workload(Node) ->
	receive
		workload -> io:format("Girus: Got workload~n"),
		girus(Node)
	end.



girus(Node) ->
	receive
		quit -> io:format("Girus: quiting...~n");
		Msg ->
			io:format("Girus: got message: ~p~n",[Msg]),
			girus(Node)
	end.


