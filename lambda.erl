%
%	Gomoku 
%	Started: October 2014
%	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-module(lambda).
-export([lambda/1
		]).

-define(NBR_EPISODES, 800). % a number of episodes lambda considers before selecting a move
-define(DEPTH, 15).
-define(DEFAUL_Q_VALUE, 0).
-define(DISCOUNT, 1.0).
-define(LAMBDA,0.2).
-define(ALPHA, 0.5).
-define(EPS, 0.05).
-define(THR,0.70).



lambda(State) ->
	case moves:get_selected_moves(State,?THR) of
		[Move] ->
			%io:format("One move selected~n"), 
			Move;
		Moves ->
			Sim_stat = [ run_episode(State,?DEPTH,[]) || _ <- lists:seq(1,?NBR_EPISODES) ],
			io:format("Lambda:Score after ~p episodes: Whites won:~p, Draws:~p, Blacks won:~p~n",
				[?NBR_EPISODES,
				lists:sum([ 1 || Score <- Sim_stat,Score =:=1 ]),
				lists:sum([ 1 || Score <- Sim_stat,Score =:=0 ]),
				lists:sum([ 1 || Score <- Sim_stat,Score =:=-1 ])
				]),
			{_,Move} = get_state_value_action(State,Moves,true),
			Move
	end.



run_episode(_,0,_) -> 0;
run_episode(State,Depth,Path) -> 
	case get_policy(State,false) of
		{random,Move} ->
			Path1 = [ {State,Move,1-?LAMBDA} ];
		{argmax,Move} ->
			Path1 = [ {State,Move,1-?LAMBDA} | Path]
	end,

	case get_next_state_reward(State,Move) of
		{blacks_won,Reward} -> learn(State,Move,blacks_won,Reward,Path1), -1;
		{whites_won,Reward} -> learn(State,Move,whites_won,Reward,Path1), 1;
		{draw,Reward} -> learn(State,Move,draw,Reward,Path1), 0;
		{Next_state,Reward} -> 
			learn(State,Move,Next_state,Reward,Path1),
			run_episode(Next_state,Depth-1,[{S,A,E*?DISCOUNT*?LAMBDA} || {S,A,E} <- Path1])
	end.



learn(State,Move,Next_state,Reward,Path) ->
	case ets:lookup(lambda,{State,Move}) of
		[{_,V}] -> V1=V;
		[] -> V1=?DEFAUL_Q_VALUE
	end,
	{V2,_} = get_state_value_action(Next_state,false),
	TD = ?ALPHA*(Reward + ?DISCOUNT * V2 - V1),
	lists:foreach(fun({S,A,E})->
		case ets:lookup(lambda,{S,A}) of
			[{_,Vs}] -> 
				Existed = true,
				New_value = Vs + TD*E;
			[] ->
				Existed = false, 
				New_value = ?DEFAUL_Q_VALUE + TD*E
		end,
		%io:format("Lambda:~p states learnt~n",[length(Path)]),
		case {Existed,abs(New_value) < 0.1} of
			{true,true} -> ets:delete(lambda, {State,Move} );
			{_,false} -> ets:insert(lambda,{ {State,Move}, New_value });
			{_,_} -> ok 
		end
				  end, Path).

	



get_state_value_action(blacks_won,_) -> {0, no_move};
get_state_value_action(whites_won,_) -> {0, no_move};	
get_state_value_action(draw,_) -> {0, no_move};
get_state_value_action(State,Print) ->
	get_state_value_action(State,moves:get_selected_moves(State,?THR),Print).

get_state_value_action({Turn,_}=State,Selected_moves,Print) ->
	Rated_moves = [ case ets:lookup(lambda, {State,Move} ) of
						[] -> {?DEFAUL_Q_VALUE,Move};
						[{{State,Move},Qa}] -> {Qa,Move}
					end || Move <- Selected_moves],
	case Turn of
		whites -> {Value,_} = lists:max(Rated_moves);
		blacks -> {Value,_} = lists:min(Rated_moves)
	end,
	case Print of
		true ->	io:format("\t * * * Other move values for ~p: ~p~n",[Turn,[ {V,M} || {V,M} <- Rated_moves] ]);
		_ -> ok
	end,
	Move = rand:pick_randomly([ Move || {V,Move} <- Rated_moves, V=:=Value]),
	{Value,Move}.

						


get_policy(State,Print) ->
	get_policy(State, moves:get_selected_moves(State,?THR),Print).

get_policy(State,Moves,Print) ->
	case rand:flip_coin(?EPS) of
		false -> {random,rand:pick_randomly(Moves)};
		_ -> 
			{_,Move} = get_state_value_action(State,Moves,Print),
			{argmax,Move}
	end.




get_next_state_reward({Turn,Board},{I,J}) ->
	case element(I,element(J,Board)) of
		e -> 
			%io:format("~nNew ~p move:(~p,~p)~n",[Turn,I,J]),
			Row1 = erlang:delete_element(I,element(J,Board)),
			Board1 = erlang:delete_element(J,Board),
			case Turn of
				whites -> 
					Row2 = erlang:insert_element(I,Row1,w),
					Next_state = {blacks, erlang:insert_element(J,Board1,Row2)};

				blacks -> 
					Row2 = erlang:insert_element(I,Row1,b),
					Next_state = {whites, erlang:insert_element(J,Board1,Row2)}
			end;
			
		_ ->
			io:format("Illegal move: ~p~n",[{I,J}]),
			Next_state = illegal_move
	end,

	% set rewards
	case lines:check_five(Next_state) of
		true -> 
			{Tn,_}=Next_state,
			case Tn of
				whites -> {blacks_won,-100};
				blacks -> {whites_won,100}
			end;
		_ -> {Next_state,0}
	end.


